function postMessage(parent, args, context, info) {
    return context.prisma.createMessage({
      body: args.body,
      likeCount: 0,
      dislikeCount: 0,
    });
  }
  
async function reactMessage(parent, args, context, info) {
  const { id, type } = args;
  const message = await context.prisma.message({ id })
  const reactType = type ? 'likeCount' : 'dislikeCount';
  const updatedMessage = await context.prisma.updateMessage({
    data: {
      [reactType]: ++message[reactType]
    },
    where: { id },
  })
  return updatedMessage;
}

  async function postReply(parent, args, context, info) {
    return context.prisma.createReply({
      body: args.body,
      likeCount: 0,
      dislikeCount: 0,
      message: { connect: { id: args.messageId } }
    });
  }
  
  async function reactReply(parent, args, context, info) {
    const { id, type } = args;
    const message = await context.prisma.reply({ id })
    const reactType = type ? 'likeCount' : 'dislikeCount';
    const updatedReply = await context.prisma.updateReply({
      data: {
        [reactType]: ++message[reactType]
      },
      where: { id },
    })
    return updatedReply;
  }

  module.exports = {
    postMessage,
    postReply,
    reactMessage,
    reactReply,
  }