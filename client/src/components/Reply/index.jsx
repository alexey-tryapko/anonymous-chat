import PropTypes from 'prop-types';
import React from 'react';
import { Mutation } from 'react-apollo';
import { Comment as ReplyUI, Icon } from 'semantic-ui-react'

import { UPDATE_REACT_REPLY_MUTATION } from '../../queries';
import styles from './styles.module.css';

const Reply = props => {
  const { id, body, likeCount, dislikeCount } = props.reply;

  return (
    <ReplyUI.Group className={styles.reply}>
      <ReplyUI>
        <ReplyUI.Content>
          <ReplyUI.Author>{id.substring(15)}</ReplyUI.Author>
          <ReplyUI.Text>{body}</ReplyUI.Text>
          <ReplyUI.Actions>
            <Mutation mutation={UPDATE_REACT_REPLY_MUTATION} variables={{ id, type: true }}>
              {reactMutation => (
                <ReplyUI.Action onClick={reactMutation}>
                  <Icon name="thumbs up" />
                  {likeCount}
                </ReplyUI.Action>
              )}
            </Mutation>
            <Mutation mutation={UPDATE_REACT_REPLY_MUTATION} variables={{ id, type: false }}>
              {reactMutation => (
                <ReplyUI.Action>
                  <Icon name="thumbs down" onClick={reactMutation}/>
                  {dislikeCount}
                </ReplyUI.Action>
              )}
            </Mutation>
          </ReplyUI.Actions>
        </ReplyUI.Content>
      </ReplyUI>
    </ReplyUI.Group>
  );
};

Reply.propTypes = {
  reply: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Reply;