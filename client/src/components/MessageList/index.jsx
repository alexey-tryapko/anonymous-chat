import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import Message from '../Message/';
import Spinner from '../Spinner';
import styles from './styles.module.css';

import {
  MESSAGE_QUERY,
  NEW_MESSAGE_SUBSCRIPTION,
  NEW_UPDATE_MESSAGE_SUBSCRIPTION,
  NEW_REPLY_SUBSCRIPTION,
  NEW_UPDATE_REPLY_SUBSCRIPTION,
} from '../../queries';

const MessageList = props => {
  const { orderBy, filter } = props;
  const _subscribeToNewMessages = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
        if (exists) return prev;

        return {...prev, messages: {
          messageList: [newMessage, ...prev.messages.messageList],
          count: prev.messages.messageList.length + 1,
          __typename: prev.messages.__typename
        }};
      }
    });
  };

  const _subscribeToNewReplies = subscribeToMore => {
    subscribeToMore({
      document: NEW_REPLY_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newReply } = subscriptionData.data;
        const { messageList } = prev.messages;
        const index = messageList.findIndex(({id}) => id === newReply.message.id);
        const exists = messageList[index].replies.find(({id}) => id === newReply.id);
        if (exists) return prev;
        messageList[index].replies.push(newReply);
        const updated = messageList;
        return {...prev, messages: {
          messageList: [...updated],
          count: prev.messages.messageList.length,
          __typename: prev.messages.__typename
        }};
      }
    });
  };

  const _subscribeToMessagesUpdate = subscribeToMore => {
    subscribeToMore({
      document: NEW_UPDATE_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { updateMessage } = subscriptionData.data;
        const updated = prev.messages.messageList.map(message => (
          message.id === updateMessage.id ? { ...message, updateMessage} : message
        ));

        return {...prev, messages: {
          messageList: [...updated],
          count: prev.messages.messageList.length,
          __typename: prev.messages.__typename
        }};
      }
    });
  };

  const _subscribeToRepliesUpdate = subscribeToMore => {
    subscribeToMore({
      document: NEW_UPDATE_REPLY_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { updateReply } = subscriptionData.data;
        const updated = prev.messages.messageList.map(message => {
          if(message.id === updateReply.message.id) {
            const { replies } = message;
            const index = replies.findIndex(({id}) => id === updateReply.id);
            replies[index] = { ...replies[index], ...updateReply};
            message.replies = replies;
            return message;
          }
          return message;
        });

        return {...prev, messages: {
          messageList: [...updated],
          count: prev.messages.messageList.length,
          __typename: prev.messages.__typename
        }};
      }
    });
  };

  return (
    <Query query={MESSAGE_QUERY} variables={{ filter, orderBy }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <Spinner />;
        if (error) return <div>Fetch error</div>;
        _subscribeToNewMessages(subscribeToMore);
        _subscribeToMessagesUpdate(subscribeToMore);
        _subscribeToNewReplies(subscribeToMore);
        _subscribeToRepliesUpdate(subscribeToMore);
        const { messages: { messageList } } = data;

        return (
          <div className={styles.messageList}>
            {messageList.map(item => {
              return <Message key={item.id} message={item}/>
            })}
          </div>
        );
      }}
    </Query>
  );
};

MessageList.propTypes = {
  orderBy: PropTypes.string.isRequired,
  filter: PropTypes.string.isRequired,
}

export default MessageList;