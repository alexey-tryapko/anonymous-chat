import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.css';
import { Input, Select } from 'semantic-ui-react'

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: '',
        };
    }

    handleKeyPress(e) {
        if (e.key !== 'Enter') return;
        const { body } = this.state;
        this.props.filterSet(body);
        this.setState({ body: '' });
    }

    handleSelectChange(e, data) {
        this.props.orderSet(data.value)
    }

    render() {
        const { body } = this.state;
        const { orderOptions } = this.props;

        return (
            <header className={styles.mainHeader}>
                <Select placeholder='Select your country' options={orderOptions} onChange={(e, data) => this.handleSelectChange(e, data)} />
                <Input
                    icon='search' 
                    value={body}
                    placeholder='Search...' 
                    onChange={ev => this.setState({ body: ev.target.value })}
                    onKeyPress={event => this.handleKeyPress(event)}
                />
            </header >
        );
    }
}

Header.propTypes = {
    filterSet: PropTypes.func.isRequired,
    orderSet: PropTypes.func.isRequired,
    orderOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Header;