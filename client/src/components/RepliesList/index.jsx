import React from 'react';
import PropTypes from 'prop-types';
import Reply from '../Reply/';

const RepliesList = ({ repliesList }) => {
  return (
    <div>
      {repliesList.map(item => {
        return <Reply key={item.id} reply={item}/>
      })}
    </div>
  );
};

RepliesList.propTypes = {
    repliesList: PropTypes.arrayOf(PropTypes.object).isRequired,
};
export default RepliesList;