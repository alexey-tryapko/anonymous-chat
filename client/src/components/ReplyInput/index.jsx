import React from 'react';
import PropTypes from 'prop-types';
import { Mutation } from 'react-apollo';
import { Form, Button, FormGroup } from 'semantic-ui-react';
import styles from './styles.module.css';
import { POST_REPLY_MUTATION } from '../../queries';

class ReplyInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: '',
        };
    }

    handleSendClick = mutation => {
        mutation();
        this.setState({ body: '' })
        this.props.closeInput();
    }

    render() {
        const { body } = this.state;
        const { messageId } = this.props;
        return (
            <Form reply className={styles.formWrapper}>
                <FormGroup widths='equal'>
                    <Form.TextArea
                        value={body}
                        placeholder="Type a message..."
                        onChange={ev => this.setState({ body: ev.target.value })}
                    />
                    <div className={styles.btnWrapper}>
                    <Mutation mutation={POST_REPLY_MUTATION} variables={{ messageId, body }}>
                        {postMutation => (
                            <Button
                                as="a" 
                                className={styles.btnSent} 
                                onClick={() => this.handleSendClick(postMutation)} 
                                disabled={!body}
                            >
                                SEND
                            </Button>
                        )}
                    </Mutation>
                    <Button as="a" className={styles.btnSent} onClick={() => this.props.closeInput()} >CANCEL</Button>
                    </div>
                    
                </FormGroup>
            </Form>
        );
    }
}


ReplyInput.propTypes = {
    messageId: PropTypes.string.isRequired,
    closeInput: PropTypes.func.isRequired,
};
export default ReplyInput;