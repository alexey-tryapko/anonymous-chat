import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { Comment as MessageUI, Icon } from 'semantic-ui-react'
import RepliesList from '../RepliesList';
import ReplyInput from '../ReplyInput';
import { UPDATE_REACT_MESSAGE_MUTATION } from '../../queries';
import styles from './styles.module.css';

const Message = props => {
  const [edit, setEdit] = useState(false);
  const { id, body, likeCount, dislikeCount, replies } = props.message;

  return (
    <div className={replies.length ? styles.messsageWithReplies : styles.messsageWithoutReplies}>
      <MessageUI.Group>
        <MessageUI>
          <MessageUI.Content>
            <MessageUI.Author>{id.substring(15)}</MessageUI.Author>
            <MessageUI.Text>{body}</MessageUI.Text>
            <MessageUI.Actions>
              <Mutation mutation={UPDATE_REACT_MESSAGE_MUTATION} variables={{ id, type: true }}>
                {reactMutation => (
                  <MessageUI.Action onClick={reactMutation}>
                    <Icon name="thumbs up" />
                    {likeCount}
                  </MessageUI.Action>
                )}
              </Mutation>
              <Mutation mutation={UPDATE_REACT_MESSAGE_MUTATION} variables={{ id, type: false }}>
                {reactMutation => (
                  <MessageUI.Action>
                    <Icon name="thumbs down" onClick={reactMutation} />
                    {dislikeCount}
                  </MessageUI.Action>
                )}
              </Mutation>
              <MessageUI.Action onClick={() => setEdit(true)}>
                Reply
            </MessageUI.Action>
            </MessageUI.Actions>
          </MessageUI.Content>
        </MessageUI>
        {
          edit && <ReplyInput messageId={id} closeInput={() => setEdit(false)}/>
        }
      </MessageUI.Group>
      <RepliesList repliesList={replies} />
    </div>

  );
};

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Message;