import React from 'react';
import { Mutation } from 'react-apollo';
import { Form, Button, FormGroup } from 'semantic-ui-react';
import styles from './styles.module.css';
import { POST_MESSAGE_MUTATION } from '../../queries';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: '',
        };
    }

    handleSendClick = mutation => {
        mutation();
        this.setState({ body: '' })
    }

    render() {
        const { body } = this.state;

        return (
            <Form reply className={styles.formWrapper}>
                <FormGroup widths='equal'>
                    <Form.TextArea
                        value={body}
                        placeholder="Type a message..."
                        onChange={ev => this.setState({ body: ev.target.value })}
                        className={styles.messageInput}
                    />
                    <Mutation mutation={POST_MESSAGE_MUTATION} variables={{ body }}>
                        {postMutation => (
                            <Button as="a" className={styles.btnSent} onClick={() => this.handleSendClick(postMutation)} disabled={!body}>SEND</Button>
                        )}
                    </Mutation>
                </FormGroup>
            </Form>
        );
    }
}

export default MessageInput;