import React from "react";
import ReactDOM from "react-dom";
import Chat from "./containers/Chat";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'

import './styles/reset.css';
import 'semantic-ui-css/semantic.min.css'
import './styles/common.css';

const wsLink = new WebSocketLink({
    uri: `ws://localhost:4000`,
    options: {
      reconnect: true
    }
});
  
const httpLink = createHttpLink({
  uri: "http://localhost:4000"
});

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  httpLink
);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

ReactDOM.render(
    <ApolloProvider client={client}>
      <Chat />
    </ApolloProvider>,
  document.getElementById("root")
);
