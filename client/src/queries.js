import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery( $filter: String, $orderBy: MessageOrderByInput) {
    messages( filter: $filter, orderBy: $orderBy ) {
      count
      messageList {
        id
        body
        likeCount
        dislikeCount
        replies {
          id
          body
          likeCount
          dislikeCount
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($body: String!) {
    postMessage(body: $body) {
      id
      body
      likeCount
      dislikeCount
      replies {
          id
          body
          likeCount
          dislikeCount
      }
    }
  }
`;

export const UPDATE_REACT_MESSAGE_MUTATION = gql`
mutation UpdateMutation($id: ID!, $type: Boolean!) {
  reactMessage(id: $id, type: $type) {
    id
    likeCount
    dislikeCount
  }
}
`;

export const POST_REPLY_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $body: String!) {
    postReply(messageId: $messageId, body: $body) {
      id
      body
      likeCount
      dislikeCount
    }
  }
`;

export const UPDATE_REACT_REPLY_MUTATION = gql`
mutation UpdateMutation($id: ID!, $type: Boolean!) {
  reactReply(id: $id, type: $type) {
    id
    likeCount
    dislikeCount
  }
}
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      body
      likeCount
      dislikeCount
      replies {
          id
          body
          likeCount
          dislikeCount
      }
    }
  }
`;

export const NEW_UPDATE_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    updateMessage {
      id
      likeCount
      dislikeCount
    }
  }
`;

export const NEW_REPLY_SUBSCRIPTION = gql`
  subscription {
    newReply {
      id
      body
      likeCount
      dislikeCount
      message {
        id
      }
    }
  }
`;

export const NEW_UPDATE_REPLY_SUBSCRIPTION = gql`
  subscription {
    updateReply {
      id
      likeCount
      dislikeCount
      message {
        id
      }
    }
  }
`;
