import React from 'react';
import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import styles from './styles.module.css';


class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: '',
      orderBy: 'createdAt_ASC'
    };
  }

  static orderOptions = [
    { key: 'likeCount_DESCf', value: 'likeCount_DESC', text: 'Likes descending' },
    { key: 'likeCount_ASC', value: 'likeCount_ASC', text: 'Likes ascending' },
    { key: 'dislikeCount_DESC', value: 'dislikeCount_DESC', text: 'Dislikes descending' },
    { key: 'dislikeCount_ASC', value: 'dislikeCount_ASC', text: 'Dislikes ascending' },
    { key: 'createdAt_DESC', value: 'createdAt_DESC', text: 'Created time descending' },
    { key: 'createdAt_ASC', value: 'createdAt_ASC', text: 'Created time ascending' },
  ];
  render() {
    const { filter, orderBy } = this.state;
    return (
      <div className={styles.mainWrapper}>
        <Header
          filterSet={filter => this.setState({filter})}
          orderSet={orderBy => this.setState({orderBy})}
          orderOptions={Chat.orderOptions}
        />
        <MessageList
          filter={filter}
          orderBy={orderBy}
        />
        <MessageInput />
      </div>
    );
  }
}

export default Chat;
